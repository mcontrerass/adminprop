$(function(){
			$("[data-toggle='tooltip']").tooltip();
			$("[data-toggle='popover']").popover();
			$('.carousel').carousel({
				interval: 2000
			});
			$('#contacto').on('show.bs.modal', function (e){
				console.log('El modal contacto empezó a mostrarse');
				$('#contactoBtn').removeClass('btn-outline-success');
				$('#contactoBtn').addClass('btn-secondary');
				$('#contactoBtn').prop('disabled', true);
			});
			$('#contacto').on('shown.bs.modal', function (e){
				console.log('El modal contacto se mostró');
			});
			$('#contacto').on('hide.bs.modal', function (e){
				console.log('El modal contacto empezó a ocultarse');
			});
			$('#contacto').on('hidden.bs.modal', function (e){
				console.log('El modal contacto se ocultó');
				$('#contactoBtn').removeClass('btn-secondary');
				$('#contactoBtn').addClass('btn-outline-success');
				$('#contactoBtn').prop('disabled', false);
			});
		});